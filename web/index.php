<?php

use Symfony\Component\Validator\Constraints as Assert;

$loader = require_once __DIR__ . '/../vendor/autoload.php';

$app = new Silex\Application();

Doctrine\Common\Annotations\AnnotationRegistry::registerLoader(array($loader, 'loadClass'));

$app['debug'] = true;

$app->register(new Silex\Provider\ValidatorServiceProvider());
$app->register(new Cocur\Slugify\Bridge\Silex\SlugifyServiceProvider());
$app->register(new \Provider\SerializerProvider());
$app->register(new \Provider\MongoClientProvider());
$app->register(new \Provider\ManagersProvider());
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__ . '/../src/Resources/views',
));

$app->get('/', function() use ($app) {
    return $app['twig']->render('base.html.twig');
});

/**
 * Check user auth.
 */
$app->get('api/auth.json', function() use ($app) {
    /** @var \MongoDB $mongo */
    $mongo = $app['mongo'];

    /** @var \Symfony\Component\HttpFoundation\Request $request */
    $request = $app['request'];

    $user = $mongo->selectCollection('users')->findOne(array(
        'username' => $request->query->get('username'),
        'password' => sha1($request->query->get('password')),
    ));

    if (!count($user)) {
        return $app->json(array('valid' => false), 404);
    }
    return $app->json(array(
        'valid' => true,
        'token' => $user['pubToken']
    ));
});

/**
 * Get user details signified by token
 */
$app->get('api/user.json', function() use ($app) {
    /** @var \MongoDB $mongo */
    $mongo = $app['mongo'];

    /** @var \Symfony\Component\HttpFoundation\Request $request */
    $request = $app['request'];

    $publicToken = $request->query->get('pubToken');

    $user = $mongo->selectCollection('users')->findOne(array(
        'pubToken' => $publicToken
    ));

    unset($user['password'], $user['_id']);

    return $app->json($user);
});

/**
 * Fetch categories
 */
$app->get('api/categories.json', function() use ($app) {
    /** @var \Manager\CategoryManager $categoryManager */
    $categoryManager = $app['category.manager'];

    /** @var \JMS\Serializer\SerializerInterface $serializer */
    $serializer = $app['serializer'];

    /** @var \Symfony\Component\HttpFoundation\Request $request */
    $request = $app['request'];

    /** @var \Manager\UserManager $userManager */
    $userManager = $app['user.manager'];
    $user = $userManager->findOneByPubToken($request->query->get('pubToken'));

    return $serializer->serialize($categoryManager->findBySeller($user), 'json');
});

/**
 * Get category
 */
$app->get('api/category.json', function() use ($app) {
    /** @var \MongoDB $mongo */
    $mongo = $app['mongo'];

    /** @var \JMS\Serializer\SerializerInterface $serializer */
    $serializer = $app['serializer'];

    /** @var \Manager\CategoryManager $categoryManager */
    $categoryManager = $app['category.manager'];

    /** @var \Manager\UserManager $userManager */
    $userManager = $app['user.manager'];

    /** @var \Symfony\Component\HttpFoundation\Request $request */
    $request = $app['request'];

    $user = $userManager->findOneByPubToken($request->query->get('pubToken'));

    if (!$user->getId()) {
        return $app->json(array('valid' => false), 404);
    }

    return $app->json(array('count' =>
        $categoryManager->findSellerCategoryByName($user, $request->query->get('title'))->count()
    ), 200);
});

/**
 * Add a new category
 */
$app->post('api/category.json', function() use ($app) {
    /** @var \MongoDB $mongo */
    $mongo = $app['mongo'];

    /** @var \JMS\Serializer\SerializerInterface $serializer */
    $serializer = $app['serializer'];

    /** @var \Manager\CategoryManager $categoryManager */
    $categoryManager = $app['category.manager'];

    /** @var \Manager\UserManager $userManager */
    $userManager = $app['user.manager'];

    /** @var \Symfony\Component\HttpFoundation\Request $request */
    $request = $app['request'];

    $categoryVo = $serializer->deserialize($serializer->serialize($request->request->get('form'), 'json'), 'Model\Category', 'json');

    $category = $categoryManager->addCategory(
        $userManager->findOneByPubToken($request->request->get('pubToken')),
        $categoryVo
    );

    return $app->json($category, 200);
});

$app->put('api/category.json/{id}', function($id) use ($app) {
    /** @var \JMS\Serializer\SerializerInterface $serializer */
    $serializer = $app['serializer'];

    /** @var \Manager\CategoryManager $categoryManager */
    $categoryManager = $app['category.manager'];

    /** @var \Symfony\Component\HttpFoundation\Request $request */
    $request = $app['request'];

    $category = $categoryManager->findOneById($id);

    $categoryJson = json_decode($request->getContent(), true);

    $categoryManager->update($category, array(
        'title' => $categoryJson['title']
    ));

    return $app->json(array('valid' => true), 200);
});

/**
 * Get category
 */
$app->get('api/category.json/{id}', function($id) use ($app) {
    /** @var \Manager\CategoryManager $categoryManager */
    $categoryManager = $app['category.manager'];

    /** @var \JMS\Serializer\SerializerInterface $serializer */
    $serializer = $app['serializer'];

    return $serializer->serialize($categoryManager->findOneById($id), 'json');
});

/**
 * Remove category
 */
$app->delete('api/category.json/{id}', function($id) use ($app) {
    /** @var \Manager\CategoryManager $categoryManager */
    $categoryManager = $app['category.manager'];

    $categoryManager->removeCategory($id);

    return $app->json(array('id' => $id), 200);
});

/**
 * Fetch all orders signified by user pubToken
 */
$app->get('api/orders.json', function() use ($app) {
    /** @var \Manager\OrderManager $orderManager */
    $orderManager = $app['order.manager'];

    /** @var \Symfony\Component\HttpFoundation\Request $request */
    $request = $app['request'];

    /** @var \Manager\UserManager $userManager */
    $userManager = $app['user.manager'];

    /** @var \JMS\Serializer\SerializerInterface $serializer */
    $serializer = $app['serializer'];

    $pubToken = $request->query->get('pubToken');

    $orderCollection = $orderManager->findBySeller($userManager->findOneByPubToken($pubToken));

    return $serializer->serialize($orderCollection, 'json');
});

/**
 * Get single order signified by id
 */
$app->get('api/order.json/{id}', function($id) use ($app) {
    /** @var \Manager\OrderManager $orderManager */
    $orderManager = $app['order.manager'];

    /** @var \JMS\Serializer\SerializerInterface $serializer */
    $serializer = $app['serializer'];

    $order = $orderManager->findOneById($id);

    return  $serializer->serialize($order, 'json');
});

/**
 * Remove order
 */
$app->delete('api/order.json/{id}', function($id) use ($app) {
    /** @var \Manager\OrderManager $orderManager */
    $orderManager = $app['order.manager'];

    $orderManager->removeOrder($id);

    return $app->json(array('id' => $id), 200);
});


/**
 * Update single order
 */
$app->put('api/order.json/{id}', function($id) use ($app) {
    /** @var \Manager\OrderManager $orderManager */
    $orderManager = $app['order.manager'];

    $order = $orderManager->findOneById($id);

    $orderContent = json_decode($app['request']->getContent(), true);

    $orderManager->update($order, array(
        'done' => $orderContent['done']
    ));

    return $app->json(array('id' => $id), 200);
});

/**
 * Post order
 */
$app->post('api/order.json', function() use ($app) {
    /** @var \Manager\UserManager $userManager */
    $userManager = $app['user.manager'];

    /** @var \Symfony\Component\HttpFoundation\Request $request */
    $request = $app['request'];

    $user = $userManager->findOneByPubToken($request->query->get('pubToken'));

    /** @var \JMS\Serializer\SerializerInterface $serializer */
    $serializer = $app['serializer'];

    $orderMap = $request->getContent();


    /** @var \Model\Order $order */
    $order = $serializer->deserialize($orderMap, 'Model\Order', 'json');
    $order->setSeller($user);

    /** @var \Manager\OrderManager $orderManager */
    $orderManager = $app['order.manager'];
    $orderManager->order($order);

    return $app->json($order);
});

/**
 * Adding new product
 */
$app->post('api/product.json', function() use ($app) {
    /** @var \Symfony\Component\HttpFoundation\Request $request */
    $request = $app['request'];

    /** @var \Manager\UserManager $userManager */
    $userManager = $app['user.manager'];

    /** @var \Manager\CategoryManager $categoryManager */
    $categoryManager = $app['category.manager'];

    /** @var \Manager\ProductManager $productManager */
    $productManager = $app['product.manager'];

    /** @var \Model\User $user */
    $user = $userManager->findOneByPubToken($request->request->get('pubToken'));

    /** @var \JMS\Serializer\SerializerInterface $serializer */
    $serializer = $app['serializer'];

    $productArray = $request->request->get('form');
    $productArray['isInStack'] = (Boolean) $productArray['isInStack'];
    $productArray['category'] = $categoryManager->findOneById($productArray['category']);

    /** @var \Model\Product $product */
    $product = $serializer->deserialize($serializer->serialize($productArray, 'json'), 'Model\Product', 'json');

    $productManager->addProduct($product);

    return $app->json($product, 200);
});

/**
 * Update existing product
 */
$app->put('api/product.json/{id}', function($id) use ($app) {
    /** @var \Symfony\Component\HttpFoundation\Request $request */
    $request = $app['request'];

    /** @var \Manager\UserManager $userManager */
    $userManager = $app['user.manager'];

    /** @var \Manager\CategoryManager $categoryManager */
    $categoryManager = $app['category.manager'];

    /** @var \Manager\ProductManager $productManager */
    $productManager = $app['product.manager'];

    /** @var \JMS\Serializer\SerializerInterface $serializer */
    $serializer = $app['serializer'];

    $productArray = json_decode($request->getContent(), true);
    $productArray['isInStack'] = (Boolean) $productArray['isInStack'];
    $productArray['category'] = $categoryManager->findOneById($productArray['category']);

    $product = $serializer->deserialize($serializer->serialize($productArray, 'json'), 'Model\Product', 'json');
    $productManager->update($product);

    return $app->json($product, 200);
});

/**
 * Product list from category
 */
$app->get('api/products.json/{category}', function($category) use ($app) {
    /** @var \Manager\UserManager $userManager */
    $userManager = $app['user.manager'];

    /** @var \Manager\CategoryManager $categoryManager */
    $categoryManager = $app['category.manager'];

    /** @var \Manager\ProductManager $productManager */
    $productManager = $app['product.manager'];

    /** @var \JMS\Serializer\SerializerInterface $serializer */
    $serializer = $app['serializer'];

    /** @var \Symfony\Component\HttpFoundation\Request $request */
    $request = $app['request'];

    /** @var \Model\User $user */
    $categoryList = array(
        $categoryManager->findOneById($category)
    );

    return $serializer->serialize($productManager->findByCategories($categoryList), 'json');
});

/**
 * Product list
 */
$app->get('api/products.json', function() use ($app) {
    /** @var \Manager\UserManager $userManager */
    $userManager = $app['user.manager'];

    /** @var \Manager\CategoryManager $categoryManager */
    $categoryManager = $app['category.manager'];

    /** @var \Manager\ProductManager $productManager */
    $productManager = $app['product.manager'];

    /** @var \JMS\Serializer\SerializerInterface $serializer */
    $serializer = $app['serializer'];

    /** @var \Symfony\Component\HttpFoundation\Request $request */
    $request = $app['request'];

    /** @var \Model\User $user */
    $user = $userManager->findOneByPubToken($request->query->get('pubToken'));
    $categoryList = $categoryManager->findBySeller($user);

    return $serializer->serialize($productManager->findByCategories($categoryList), 'json');
});

/**
 * Delete product signified by id
 */
$app->delete('api/product.json/{id}', function($id) use ($app) {
    /** @var \Manager\ProductManager $productManager */
    $productManager = $app['product.manager'];

    $productManager->remove($id);

    return $app->json(array('status' => 'ok'));
});

/**
 * Get product details
 */
$app->get('api/product.json/{id}', function($id) use ($app) {
    /** @var \Manager\ProductManager $productManager */
    $productManager = $app['product.manager'];

    /** @var \JMS\Serializer\SerializerInterface $serializer */
    $serializer = $app['serializer'];

    $productTransformer = new \DataTransformer\ProductTransformer($productManager->findById($id));
    $transformedProduct = $productTransformer->transformJsonSingleRow();

    return $serializer->serialize($transformedProduct, 'json');
});

$app->get('api/cart.json/{ids}', function($ids) use ($app) {
    /** @var \Manager\ProductManager $productManager */
    $productManager = $app['product.manager'];

    /** @var \JMS\Serializer\SerializerInterface $serializer */
    $serializer = $app['serializer'];

    return $serializer->serialize($productManager->findByIds(explode(',', $ids)), 'json');
});

$app->get('api/featured.json', function() use ($app) {
    /** @var \Manager\UserManager $userManager */
    $userManager = $app['user.manager'];

    /** @var \Manager\CategoryManager $categoryManager */
    $categoryManager = $app['category.manager'];

    /** @var \Manager\ProductManager $productManager */
    $productManager = $app['product.manager'];

    /** @var \JMS\Serializer\SerializerInterface $serializer */
    $serializer = $app['serializer'];

    /** @var \Symfony\Component\HttpFoundation\Request $request */
    $request = $app['request'];

    /** @var \Model\User $user */
    $user = $userManager->findOneByPubToken($request->query->get('pubToken'));
    $categoryList = $categoryManager->findFeaturedBySeller($user);

    return $serializer->serialize($productManager->findByCategories($categoryList), 'json');
});

$app->run();
