require.config({
    baseUrl: '/assets/js',
    paths: {
        'jquery': '../bower_components/jquery/dist/jquery.min',
        'underscore': '../bower_components/underscore-amd/underscore-min',
        'backbone': '../bower_components/backbone-amd/backbone-min',
        'backbone-forms': '../bower_components/backbone-forms/distribution.amd/backbone-forms',
        'backbone-forms-bootstrap': '../bower_components/backbone-forms/distribution.amd/templates/bootstrap3',
        'authenticationService': 'services/AuthenticationService',
        'formService': 'services/FormService'
    },
    shim: {
        'app': {
            deps: ['jquery', 'backbone-forms-bootstrap']
        },
        'jquery': {
            export: '$'
        },
        'formService': {
            deps: ['backbone-forms']
        },
        'backbone-forms': {
            deps: ['backbone'],
            export: 'Form'
        },
        'backbone-forms-bootstrap': {
            deps: [
                'backbone-forms'
            ]
        }
    },
    deps: ['main']
});
