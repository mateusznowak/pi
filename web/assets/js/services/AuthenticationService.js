define([
    'app'
], function(
    App
) {
    var authenticationService = {};

    authenticationService.authenticate = function(token) {
        localStorage.setItem("authenticationToken", token);
    };

    authenticationService.getToken = function() {
        return localStorage.getItem("authenticationToken");
    };

    authenticationService.isAuthenticated = function() {
        return localStorage.getItem("authenticationToken") != null;
    };

    authenticationService.logout = function() {
        localStorage.removeItem("authenticationToken");
    };

    authenticationService.assertAuthenticated = function() {
        if (this.isAuthenticated() == false) {
            throw "Not authenticated";
        }
    };

    authenticationService.authenticationPromise = function() {
        var promise = $.Deferred();

        if (authenticationService.isAuthenticated() == false) {
            promise.reject();
        }

        $.get(App.URI_USER, { 'pubToken': authenticationService.getToken() }, function(user) {
            promise.resolve(user);
        });

        return promise;
    };

    return authenticationService;
});
