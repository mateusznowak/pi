define([
    'backbone-forms',
], function(
    BackboneForms
) {
    return function (attributes) {
        var form = new Backbone.Form(attributes);

        return form;
    };
});
