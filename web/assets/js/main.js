define([
    'app',
    'backbone',
    'views/AuthView',
    'views/DashboardView',
    'views/DashboardMainView',
    'views/DashboardOrdersListView',
    'views/DashboardCategoriesListView',
    'views/DashboardCategoryEditView',
    'views/DashboardCategoryAddView',
    'views/DashboardProductsListView',
    'views/DashboardProductEditView',
    'views/DashboardProductAddView',
    'views/DashboardOrdersListView',
    'models/CategoryModel',
    'models/ProductModel',
    'models/OrderModel',
    'authenticationService',
    'views/FlashMessageView'
], function(
    App,
    Backbone,
    AuthView,
    DashboardView,
    DashboardMainView,
    DashboardOrdersListView,
    DashboardCategoriesListView,
    DashboardCategoryEditView,
    DashboardCategoryAddView,
    DashboardProductsListView,
    DashboardProductEditView,
    DashboardProductAddView,
    DashboardOrdersListView,
    CategoryModel,
    ProductModel,
    OrderModel,
    AuthenticationService,
    FlashMessageView
) {

    var BackboneRouter = Backbone.Router.extend({
        routes: {
            "dashboard": "dashboard",

            // Categories
            "categories": "categoriesList",
            "categories/add": "categoriesAdd",
            "categories/:id/remove": "categoryRemove",
            "categories/:id/edit": "categoryEdit",

            // Products
            "products": "productsList",
            "products/add": "productsAdd",
            "products/:id/remove": "productRemove",
            "products/:id/edit": "productEdit",

            // Orders
            "orders": "ordersList",
            "orders/:id/realise": "orderRealize",
            "orders/:id/remove": "orderRemove",
            
            "logout": "logout",
            "*path": "main"
        },

        ordersList: function() {
            AuthenticationService.assertAuthenticated();

            new DashboardOrdersListView();
        },
        
        orderRealize: function(orderId) {
            AuthenticationService.assertAuthenticated();

            var orderModel = new OrderModel({ id: orderId });
            orderModel.fetch().done(function() {
                orderModel.set('done', true);
                orderModel.save();

                new FlashMessageView({
                    attributes: {
                        message: 'Zamówienie zostało oznaczone jako zrealizowane',
                        back: 'orders'
                    }
                });
            });
        },

        orderRemove: function(orderId) {
            AuthenticationService.assertAuthenticated();

            var orderModel = new OrderModel({ 'id': orderId });

            orderModel.destroy({
                success: function() {
                    new FlashMessageView({
                        attributes: {
                            message: 'Usuwanie zakończone powodzeniem',
                            back: 'orders'
                        }
                    });
                }
            });
        },

        productsList: function() {
            AuthenticationService.assertAuthenticated();

            new DashboardProductsListView();
        },

        productRemove: function(productId) {
            var productModel = new ProductModel({ id: productId });

            productModel.destroy({ 
                success: function() {
                    new FlashMessageView({
                        attributes: {
                            message: 'Usuwanie zakończone powodzeniem',
                            back: 'products'
                        }
                    });
                }
            });
        },

        productEdit: function(productId) {
            var productModel = new ProductModel({ id: productId });
            productModel.fetch().done(function() {
                new DashboardProductEditView({
                    model: productModel
                });
            });
        },

        categoriesList: function() {
            AuthenticationService.assertAuthenticated();

            return new DashboardCategoriesListView();
        },

        categoriesAdd: function() {
            AuthenticationService.assertAuthenticated();

            new DashboardCategoryAddView({
                model: new CategoryModel()
            });
        },

        productsAdd: function() {
            AuthenticationService.assertAuthenticated();

            new DashboardProductAddView({
                model: new ProductModel()
            });
        },

        categoryRemove: function(categoryId) {
            var categoryModel = new CategoryModel({ 'id': categoryId });

            categoryModel.destroy({
                success: function() {
                    new FlashMessageView({
                        attributes: {
                            message: 'Usuwanie zakończone powodzeniem',
                            back: 'categories'
                        }
                    });
                }
            });
        },

        categoryEdit: function(categoryId) {
            var categoryModel = new CategoryModel({ id: categoryId });
            categoryModel.fetch().done(function() {
                new DashboardCategoryEditView({
                    model: categoryModel
                });
            });
        },

        logout: function() {
            AuthenticationService.logout();

            Backbone.history.navigate('', { trigger: true });
        },

        dashboard: function() {
            AuthenticationService.assertAuthenticated();

            new DashboardMainView();
        },

        main: function() {
            if (AuthenticationService.isAuthenticated()) {
                return Backbone.history.navigate('dashboard', { trigger: true });
            }

            new AuthView();
        }
    });

    new BackboneRouter();

    Backbone.history.start();

    return;
});
