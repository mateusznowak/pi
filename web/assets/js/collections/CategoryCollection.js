define([
    'backbone',
    'app',
    'models/CategoryModel'
], function(
    Backbone,
    App,
    CategoryModel
) {
    return Backbone.Collection.extend({
        url: App.URI_CATEGORIES,
        model: CategoryModel
    });
});
