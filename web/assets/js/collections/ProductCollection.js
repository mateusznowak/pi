define([
    'backbone',
    'app'
], function(
    Backbone,
    App
) {
    return Backbone.Collection.extend({
        url: App.URI_PRODUCTS
    });
});

