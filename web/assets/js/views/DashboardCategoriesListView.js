define([
    'app',
    'backbone',
    'authenticationService',
    'views/DashboardView',
    'collections/CategoryCollection'
], function(
    App,
    Backbone,
    AuthenticationService,
    DashboardView,
    CategoryCollection
) {
    return Backbone.View.extend({
        events: {
            'click .btn-confirm': function(e) {
                if (confirm('Czy na pewno chcesz to zrobić?') == false) {
                    e.stopPropagation();
                    e.preventDefault();
                    return;
                }
            }
        },

        initialize: function() {
            var self = this;
            var parentView = new DashboardView();

            parentView.on('parentView.ready', function() {
                self.render();

                App.hideLoader();
            });

            this.parentView = parentView;
        },

        render: function() {
            var self = this;
            var categoryCollection = new CategoryCollection();
            categoryCollection.url = App.URI_CATEGORIES + "?pubToken=" + AuthenticationService.getToken();

            var renderTemplate = function(categoryList) {
                App.getTemplate('categories/list.html')
                    .then(function(template) {
                        self.$el.append(_.template(template, {
                            'categories': categoryList.toJSON()   
                        }));
                    })
                    .then(function() {
                        var $container = $('.spa-dashboard-container'); 

                        if ($container.length == 0) {
                            throw "Container not found";
                        }

                        self.$el.appendTo($container);
                        self.initBreadcrumb();

                        App.hideLoader();
                    });
            };

            categoryCollection.fetch().done(function() {
                renderTemplate(categoryCollection);
            });
        },

        initBreadcrumb: function() {
            var breadcrumb = [
                {
                    'route': '#categories', 
                    'title': 'Lista kategorii'
                }
            ];

            this.parentView.trigger('parentView.breadcrumb.ready', breadcrumb);
        }
    });
});
