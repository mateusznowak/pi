define([
    'backbone',
    'app',
    'authenticationService',
], function(
    Backbone,
    App,
    AuthenticationService
) {
    return Backbone.View.extend({
        initialize: function() {
            this.render();
        },

        render: function() {
            var templatePromiseChain = App.getTemplate('breadcrumb.html');
            var self = this;

            App.showLoader();

            templatePromiseChain.then(function(template) {
                self.$el.append(_.template(template, {
                    breadcrumb: self.attributes.breadcrumb   
                }));
            })
            .then(function() {
                var $breadcrumb = $('.spa-breadcrumb');

                if ($breadcrumb.length == 0) {
                    throw "No breadcrumb set";
                }

                self.$el.appendTo($breadcrumb);

            });
        }
    });
});

