define([
    'app',
    'backbone',
    'authenticationService',
    'views/AuthPanelView',
    'views/BreadcrumbView'
], function(
    App,
    Backbone,
    AuthenticationService,
    AuthPanelView,
    BreadcrumbView
) {
    return Backbone.View.extend({
        initialize: function() {
            this.render();
        },

        render: function() {
            var self = this;

            App.getTemplate('dashboard.html')
                .then(function(template) {
                    self.$el.append(_.template(template));
                    self.on('parentView.breadcrumb.ready', function(breadcrumb) {
                        self.breadcrumbView = new BreadcrumbView({
                            attributes: {
                                "breadcrumb": breadcrumb
                            }
                        });
                    });
                })
                .then(function() {
                    App.switchView(self.$el);
                })
                .then(function() {
                    self.authPanelView = new AuthPanelView();
                })
                .then(function() {
                    self.trigger('parentView.ready');
                });
        }
    });
});
