define([
    'app',
    'backbone',
    'formService',
    'authenticationService',
    'views/DashboardView',
    'collections/CategoryCollection',
    'models/ProductModel',
    'views/FlashMessageView'
], function(
    App,
    Backbone,
    FormService,
    AuthenticationService,
    DashboardView,
    CategoryCollection,
    ProductModel,
    FlashMessageView
) {
    return Backbone.View.extend({
        events: {
            'click .btn-confirm': function(e) {
                if (confirm('Czy na pewno chcesz to zrobić?') == false) {
                    e.stopPropagation();
                    e.preventDefault();
                    return;
                }
            },
            'submit form': 'submitForm'
        },

        submitForm: function(e) {
            var errorList = this.form.commit({ validate: true });

            if (errorList == null) {

                App.showLoader();
                var formValues = this.form.getValue();

                $.post(App.URI_PRODUCT, {
                        'form': formValues,
                        'pubToken': AuthenticationService.getToken()
                    })
                    .done(function() {
                        new FlashMessageView({
                            attributes: {
                                message: 'Dodawanie produktu przebiegło pomyślnie',
                                back: 'products'
                            }
                        });
                    });
            }

            e.preventDefault();
            e.stopPropagation();
        },

        initialize: function() {
            var self = this;
            var parentView = new DashboardView();

            parentView.on('parentView.ready', function() {
                self.render();

                App.hideLoader();
            });

            this.parentView = parentView;
        },

        render: function() {
            var self = this;

            App.getTemplate('products/add.html')
                .then(function(template) {
                    var form = new FormService({
                        model: self.model
                    }).render();

                    self.form = form;
                    self.$el.append(_.template(template, {}));
                    self.$el.find('.form-area').append(self.form.$el);
                })
                .then(function() {
                    var $container = $('.spa-dashboard-container');

                    if ($container.length == 0) {
                        throw "Container not found";
                    }

                    self.$el.appendTo($container);
                    self.initBreadcrumb();
                });
        },

        initBreadcrumb: function() {
            var breadcrumb = [
                {
                    'route': '#products',
                    'title': 'Lista produktów'
                },
                {
                    'route': '#products/add',
                    'title': 'Dodawanie produktu'
                }
            ];

            this.parentView.trigger('parentView.breadcrumb.ready', breadcrumb);
        }
    });
});


