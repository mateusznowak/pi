define([
    'app',
    'backbone',
    'authenticationService',
    'views/DashboardView'
], function(
    App,
    Backbone,
    AuthenticationService,
    DashboardView 
) {
    return Backbone.View.extend({
        initialize: function() {
            var self = this;
            var parentView = new DashboardView();

            parentView.on('parentView.ready', function() {
                self.render();

                App.hideLoader();
            });

            this.parentView = parentView;
        },

        render: function() {
            var self = this;

            App.getTemplate('dashboard-main.html')
                .then(function(template) {
                    self.$el.append(_.template(template));
                })
                .then(function() {
                    var $container = $('.spa-dashboard-container'); 

                    if ($container.length == 0) {
                        throw "Container not found";
                    }

                    self.$el.appendTo($container);
                    self.initBreadcrumb();
                });
        },

        initBreadcrumb: function() {
            var breadcrumb = [{}];

            this.parentView.trigger('parentView.breadcrumb.ready', breadcrumb);
        }
    });
});
