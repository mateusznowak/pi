define([
    'app',
    'backbone',
    'authenticationService',
    'views/DashboardView',
    'collections/OrderCollection'
], function(
    App,
    Backbone,
    AuthenticationService,
    DashboardView,
    OrderCollection
) {
    return Backbone.View.extend({
        events: {
            'click .btn-confirm': function(e) {
                if (confirm('Czy na pewno chcesz to zrobić?') == false) {
                    e.stopPropagation();
                    e.preventDefault();
                    return;
                }
            }
        },

        initialize: function() {
            var self = this;
            var parentView = new DashboardView();

            parentView.on('parentView.ready', function() {
                self.render();

                App.hideLoader();
            });

            this.parentView = parentView;
        },

        render: function() {
            var orderCollection = new OrderCollection();
            orderCollection.url = App.URI_ORDERS + "?pubToken=" + AuthenticationService.getToken();
            var self = this;

            var renderTemplate = function(orderList) { 
                App.getTemplate('orders/list.html')
                    .then(function(template) {
                        self.$el.append(_.template(template, {
                            'orders': orderList.toJSON()   
                        }));
                    })
                    .then(function() {
                        var $container = $('.spa-dashboard-container'); 

                        if ($container.length == 0) {
                            throw "Container not found";
                        }

                        self.$el.appendTo($container);
                        self.initBreadcrumb();
                        App.hideLoader();
                    });
            };

            orderCollection.fetch().done(function() {
                renderTemplate(orderCollection);
            });
        },

        initBreadcrumb: function() {
            var breadcrumb = [
                {
                    'route': '#orders', 
                    'title': 'Lista wszystkich zamówień'
                }
            ];

            this.parentView.trigger('parentView.breadcrumb.ready', breadcrumb);
        }
    });
});
