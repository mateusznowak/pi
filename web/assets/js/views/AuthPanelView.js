define([
    'app',
    'backbone',
    'authenticationService',
    'views/AuthPanelView'
], function(
    App,
    Backbone,
    AuthenticationService,
    AuthPanelView
) {
    return Backbone.View.extend({
        initialize: function() {
            this.render();
        },

        render: function() {
            App.showLoader();
            
            var self = this;
            var renderTemplate = function(user) {
                App.getTemplate('auth-panel.html')
                    .then(function(template) {
                        self.$el.empty();
                        self.$el.append(_.template(template, {
                            'user': user   
                        }));
                    })
                    .then(function() {
                        var $panel = $('.spa-auth-panel');

                        if ($panel.length == 0) {
                            throw "Auth panel container not found";
                        }

                        $panel.empty();
                        self.$el.appendTo($panel);
                    });
            };

            AuthenticationService
                .authenticationPromise()
                    .then(function(user) {
                         renderTemplate(user);
                    })
                    .fail(function() {
                        throw "Problem z uwierzytelnieniem użytkownika.";
                    });
        }
    });
});
