define([
    'app',
    'backbone',
    'authenticationService',
    'views/DashboardView'
], function(
    App,
    Backbone,
    AuthenticationService,
    DashboardView
) {
    return Backbone.View.extend({
        initialize: function() {
            var self = this;
            var parentView = new DashboardView();

            parentView.on('parentView.ready', function() {
                self.render();

                App.hideLoader();
            });

            this.parentView = parentView;
        },

        render: function() {
            var self = this;

            App.getTemplate('flash.html')
                .then(function(template) {
                    self.$el.append(_.template(template, {
                        'message': self.attributes.message,
                        'back': self.attributes.back
                    }));
                })
                .then(function() {
                    var $container = $('.spa-dashboard-container');

                    if ($container.length == 0) {
                        throw "Container not found";
                    }

                    self.$el.appendTo($container);

                    App.hideLoader();
                });
        }
    });
});
