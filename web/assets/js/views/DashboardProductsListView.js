define([
    'app',
    'backbone',
    'authenticationService',
    'views/DashboardView',
    'collections/ProductCollection'
], function(
    App,
    Backbone,
    AuthenticationService,
    DashboardView,
    ProductCollection
) {
    return Backbone.View.extend({
        events: {
            'click .btn-confirm': function(e) {
                if (confirm('Czy na pewno chcesz to zrobić?') == false) {
                    e.stopPropagation();
                    e.preventDefault();
                    return;
                }
            }
        },

        initialize: function() {
            var self = this;
            var parentView = new DashboardView();

            parentView.on('parentView.ready', function() {
                self.render();

                App.hideLoader();
            });

            this.parentView = parentView;
        },

        render: function() {
            var productCollection = new ProductCollection();
            productCollection.url = App.URI_PRODUCTS + "?pubToken=" + AuthenticationService.getToken();
            var self = this;

            var renderTemplate = function(productList) { 
                App.getTemplate('products/list.html')
                    .then(function(template) {
                        self.$el.append(_.template(template, {
                            'products': productList.toJSON()   
                        }));
                    })
                    .then(function() {
                        var $container = $('.spa-dashboard-container'); 

                        if ($container.length == 0) {
                            throw "Container not found";
                        }

                        self.$el.appendTo($container);
                        self.initBreadcrumb();
                        App.hideLoader();
                    });
            };

            productCollection.fetch().done(function() {
                renderTemplate(productCollection);
            });
        },

        initBreadcrumb: function() {
            var breadcrumb = [
                {
                    'route': '#products', 
                    'title': 'Lista produktów'
                }
            ];

            this.parentView.trigger('parentView.breadcrumb.ready', breadcrumb);
        }
    });
});

