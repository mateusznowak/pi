define([
    'backbone',
    'app',
    'authenticationService'
], function(
    Backbone,
    App,
    AuthenticationService
) {
    return Backbone.View.extend({
        events: {
            "submit form": "tryAuthenticate"
        },

        initialize: function() {
            this.render();
        },

        tryAuthenticate: function(e) {
            $.getJSON(App.URI_AUTH, {
                    'username': $('#inputUsername').val(),
                    'password': $('#inputPassword1').val()
                })
                .done(function(response) {
                    AuthenticationService.authenticate(response.token);
                    Backbone.history.navigate('dashboard', { trigger: true });
                })
                .fail(function() {
                    App.flash('danger', 'Nieprawidłowe dane.');
                });


            e.preventDefault();
            e.stopPropagation();
        },

        render: function() {
            var templatePromiseChain = App.getTemplate('auth.html');
            var self = this;

            App.showLoader();

            templatePromiseChain.then(function(template) {
                self.$el.append(_.template(template));
            }).then(function() {
                App.switchView(self.$el);
            }).then(function() {
                App.hideLoader();  
            });
        }
    });
});
