define([], function() {
    var app = {};

    app.TOKEN = null;
    app.URI_PREFIX = '/index.php';
    app.$container = $('.spa-container');
    app.$loader = $('.spa-loader');
    app.$flash = function() {
        return $('.spa-flash');
    };

    app.URI_AUTH = app.URI_PREFIX + '/api/auth.json';
    app.URI_USER = app.URI_PREFIX + '/api/user.json';
    app.URI_CATEGORIES = app.URI_PREFIX + '/api/categories.json';
    app.URI_CATEGORY = app.URI_PREFIX + '/api/category.json';
    app.URI_CATEGORY_NAME_CHECK = app.URI_PREFIX + '/api/category.json';
    app.URI_PRODUCTS = app.URI_PREFIX + '/api/products.json';
    app.URI_PRODUCT = app.URI_PREFIX + '/api/product.json';
    app.URI_ORDER = app.URI_PREFIX + '/api/order.json';
    app.URI_ORDERS = app.URI_PREFIX + '/api/orders.json';

    app.getTemplate = function(template) {
        return $.get('/assets/js/templates/' + template);
    };

    app.flash = function(type, message) {
        var htmlTemplate = this.getTemplate('error-type.html');
        
        htmlTemplate.then(function(response) {
            var template = _.template(response, {
                type: type,
                message: message
            });

            app.$flash().hide().append(template).toggle('slow');
        }).then(function() {
            setTimeout(function() {
                app.$flash().empty();
            }, 5000);  
        });
    };

    app.switchView = function($el) {
        app.$container.empty();
        $el.appendTo(app.$container);
    };

    app.showLoader = function() {
        app.$loader.show();
    };

    app.hideLoader = function() {
        // mocked
        //
        setTimeout(function() {
            app.$loader.hide();
        }, 500);
    };

    return app;
});
