define([
    'backbone',
    'app',
    'collections/CategoryCollection',
    'authenticationService'
], function(
    Backbone,
    App,
    CategoryCollection,
    AuthenticationService
) {
    return Backbone.Model.extend({
        urlRoot: App.URI_PRODUCT,
        schema: {
            title: {
                type: 'Text',
                title: 'Nazwa produktu',
                validators: [
                    { type: 'required', 'message': 'Ta wartość nie może być pusta' }
                ] 
            },
            description: {
                type: 'TextArea',
                title: 'Opis produktu',
                validators: [
                    { type: 'required', 'message': 'Ta wartość nie może być pusta' }
                ] 
            },
            category: {
                type: 'Select', 
                title: 'Kategoria',
                options: function(callback, editor) {
                    var categoryCollection = new CategoryCollection();
                    categoryCollection.url = App.URI_CATEGORIES + "?pubToken=" + AuthenticationService.getToken();

                    categoryCollection.fetch().done(function() {
                        return callback(categoryCollection);
                    });
                },
                validators: [
                    { type: 'required', 'message': 'Ta wartość nie może być pusta' }
                ] 
            },
            image: {
                type: 'Text',
                title: 'URL do zdjęcia',
                validators: [
                    { type: 'required', 'message': 'Ta wartość nie może być pusta' },
                    { type: 'url', message: 'Niepoprawny URL' }
                ]
            },
            price: {
                type: 'Number',
                title: 'Cena [pln]'
            },
            isInStack: {
                type: 'Select',
                title: 'Jest na stanie?',
                options: {
                    1: 'Tak',
                    0: 'Nie'
                }
            }
        }
    });
});

