define([
    'backbone',
    'app',
    'authenticationService'
], function(
    Backbone,
    App,
    AuthenticationService
) {
    return Backbone.Model.extend({
        urlRoot: App.URI_CATEGORY,
        schema: {
            title: {
                type: 'Text',
                title: 'Nazwa kategorii',
                validators: [
                    { type: 'required', 'message': 'Ta wartość nie może być pusta' }
                ] 
            }
        },
        validate: function(attrs) {
            var errors = {};

            $.ajaxSetup({
                async: false
            });

            App.showLoader();

            $.getJSON(App.URI_CATEGORY_NAME_CHECK, {
                    title: attrs.title,
                    pubToken: AuthenticationService.getToken()
                })
                .done(function(res) {
                    App.hideLoader();

                    if (res.count > 0) {
                        errors.title = "Taka kategoria już istnieje";
                    }
                });

            $.ajaxSetup({
                async: true
            });

            return errors;
        },
        toString: function() {
            return this.get('title');
        }
    });
});
