define([
    'backbone',
    'app'
], function(
    Backbone,
    App
) {
    return Backbone.Model.extend({
        urlRoot: App.URI_ORDER
    });
});

