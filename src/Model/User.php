<?php

namespace Model;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Type;

/**
 * Class User
 * @ExclusionPolicy("all")
 */
class User
{
    /**
     * @Expose
     * @Type("string")
     */
    protected $id;

    /** @Type("string") */
    protected $username;

    /** @Type("string") */
    protected $password;
    /**
     * @Expose
     * @Type("string")
     */
    protected $surname;
    /**
     * @Expose
     * @Type("string")
     */
    protected $lastname;

    /** @Type("string") */
    protected $pubToken;

    /**
     * @param \String $id
     * @param \String $username
     * @param \String $password
     * @param \String $surname
     * @param \String $lastname
     * @param \String $pubToken
     */
    public function __construct($id, $username, $password, $surname, $lastname, $pubToken)
    {
        $this->id = $id;
        $this->username = $username;
        $this->password = $password;
        $this->surname = $surname;
        $this->lastname = $lastname;
        $this->pubToken = $pubToken;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getSurname()
    {
        return $this->surname;
    }

    public function getLastname()
    {
        return $this->lastname;
    }

    public function getPubToken()
    {
        return $this->pubToken;
    }
}
