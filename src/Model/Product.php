<?php

namespace Model;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;

class Product
{
    /** @Type("string") */
    protected $id;

    /**
     * @Type("boolean")
     * @SerializedName("isInStack")
     */
    protected $isInStack;

    /** @Type("string") */
    protected $title;

    /** @Type("float") */
    protected $price;

    /** @Type("string") */
    protected $description;

    /** @Type("string") */
    protected $image;

    /** @Type("Model\Category") */
    protected $category;

    public function __construct($id, $isInStack, $title, $description, $price, $image, Category $category)
    {
        $this->id = $id;
        $this->isInStack = $isInStack;
        $this->title = $title;
        $this->description = $description;
        $this->price = $price;
        $this->image = $image;
        $this->category = $category;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getIsInStack()
    {
        return $this->isInStack;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function getPrice()
    {
        return (Float) $this->price;
    }
}
