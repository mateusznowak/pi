<?php

namespace Model;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;

class Order
{
    /** @Type("string") */
    protected $id;

    /** @Type("string") */
    protected $name;

    /** @Type("string") */
    protected $email;

    /** @Type("string") */
    protected $phone;

    /** @Type("string") */
    protected $address;

    /** @Type("boolean") */
    protected $done = false;

    /** @Type("array")
     *  @SerializedName("product_refs")
     **/
    protected $products = array();

    /** @Type("string")
     *  @SerializedName("api_client_name")
     **/
    protected $source;

    /** @Type("Model\User") */
    protected $seller;

    public function __construct($id, $name, $email, $phone, $address, $done, array $products, User $seller)
    {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->phone = $phone;
        $this->address = $address;
        $this->products = $products;
        $this->done = $done;
        $this->seller = $seller;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function isDone()
    {
        return (Boolean) $this->done;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function getSource()
    {
        return $this->source;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function getProducts()
    {
        return $this->products;
    }

    public function getSeller()
    {
        return $this->seller;
    }

    public function setSeller(User $seller)
    {
        $this->seller = $seller;

        return $this;
    }
}
