<?php

namespace Model;

use JMS\Serializer\Annotation\Type;

class Category
{
    /** @Type("string") */
    protected $id;

    /** @Type("string") */
    protected $title;

    /** @Type("string") */
    protected $slug;

    /** @Type("Model\User") */
    protected $user;

    public function __construct($id, $title, $slug, User $user)
    {
        $this->id = $id;
        $this->title = $title;
        $this->slug = $slug;
        $this->user = $user;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
    }

    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function getUser()
    {
        return $this->user;
    }
}