<?php

namespace DataTransformer;

use Model\Product;

class ProductTransformer
{
    protected $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function transformMongoJson()
    {
        return array(
            'title' => $this->product->getTitle(),
            'description' => $this->product->getDescription(),
            'price' => $this->product->getPrice(),
            'isInStack' => $this->product->getIsInStack(),
            'image' => $this->product->getImage(),
            'category' => new \MongoId($this->product->getCategory()->getId())
        );
    }

    public function transformJsonSingleRow()
    {
        return array(
            'id' => $this->product->getId(),
            'title' => $this->product->getTitle(),
            'description' => $this->product->getDescription(),
            'price' => $this->product->getPrice(),
            'isInStack' => (Integer) $this->product->getIsInStack(),
            'image' => $this->product->getImage(),
            'category' => $this->product->getCategory()->getId()
        );
    }
}