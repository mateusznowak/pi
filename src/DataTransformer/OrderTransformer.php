<?php

namespace DataTransformer;

use Model\Order;

class OrderTransformer
{
    protected $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function transformToMongoJson()
    {
        $productList = array();
        foreach ($this->order->getProducts() as $product) {
            $productList[] = new \MongoId($product);
        }


        return array(
            'name' => $this->order->getName(),
            'email' => $this->order->getEmail(),
            'phone' => $this->order->getPhone(),
            'address' => $this->order->getAddress(),
            'source' => $this->order->getSource(),
            'products' => $productList,
            'seller' => new \MongoId($this->order->getSeller()->getId()),
            'done' => false,
        );
    }
}