<?php

namespace DataTransformer;

use Model\Category;

class CategoryTransformer
{
    protected $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function transformMongoJson()
    {
        return array(
            'title' => $this->category->getTitle(),
            'slug' => $this->category->getSlug(),
            'userRef' => new \MongoId($this->category->getUser()->getId())
        );
    }
}