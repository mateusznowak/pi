<?php

namespace Provider;

use Silex\Application;
use Silex\ServiceProviderInterface;

class MongoClientProvider implements ServiceProviderInterface
{
    public function register(Application $app)
    {
        $app['mongo'] = $app->share(function() use ($app) {
            $mongo = new \Mongo();
            return $mongo->selectDb('hurtadmin');
        });
    }

    public function boot(Application $app)
    {
    }
}