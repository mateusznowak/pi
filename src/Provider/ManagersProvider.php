<?php

namespace Provider;

use Manager\CategoryManager;
use Manager\OrderManager;
use Manager\ProductManager;
use Manager\UserManager;
use Silex\Application;
use Silex\ServiceProviderInterface;

class ManagersProvider implements ServiceProviderInterface
{
    public function register(Application $app)
    {
        $app['user.manager'] = new UserManager($app['mongo']);
        $app['category.manager'] = new CategoryManager($app['mongo'], $app['user.manager'], $app['slugify']);
        $app['product.manager'] = new ProductManager($app['mongo'], $app['user.manager'], $app['category.manager']);
        $app['order.manager'] = new OrderManager($app['mongo'], $app['user.manager'], $app['product.manager']);
    }

    public function boot(Application $app)
    {
    }
}