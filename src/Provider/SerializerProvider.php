<?php
namespace Provider;

use Silex\Application;
use Silex\ServiceProviderInterface;

class SerializerProvider implements ServiceProviderInterface
{
    public function register(Application $app)
    {
        $app['serializer'] = $app->share(function () use ($app) {
            return \JMS\Serializer\SerializerBuilder::create()->build();
        });
    }

    public function boot(Application $app)
    {
    }
}
