<?php

namespace Manager;

use DataTransformer\ProductTransformer;
use Model\Category;
use Model\Product;

class ProductManager
{
    /** @var \MongoDB */
    protected $mongoClient;

    /** @var UserManager $userManager */
    protected $userManager;

    /** @var CategoryManager $categoryManager */
    protected $categoryManager;

    public function __construct(\MongoDB $mongoClient, UserManager $userManager, CategoryManager $categoryManager)
    {
        $this->mongoClient = $mongoClient;
        $this->userManager = $userManager;
        $this->categoryManager = $categoryManager;
    }

    public function remove($productId)
    {
        return $this->getMongoCollection()->remove(array('_id' => new \MongoId($productId)));
    }

    public function update(Product $product)
    {
        $productId = new \MongoId($product->getId());
        $productTransformer = new ProductTransformer($product);

        return $this->getMongoCollection()->update(array('_id' => $productId), array('$set' => $productTransformer->transformMongoJson()));
    }

    public function findByIds(array $ids)
    {
        $products = array();

        foreach ($ids as $id) {
            $product = $this->findById($id);

            if ($product->getId() == $id) {
                $products[] = $product;
            }
        }

        return $products;
    }

    public function findById($id)
    {
        $product = $this->getMongoCollection()->findOne(array(
            '_id' => new \MongoId($id)
        ));

        return new Product(
            (string) $product['_id'], $product['isInStack'], $product['title'], $product['description'],
            $product['price'], $product['image'], $this->categoryManager->findOneById($product['category'])
        );
    }

    public function findByCategories(array $categories)
    {
        $categoryIds = array();
        foreach ($categories as $category) {
            $categoryIds[] = new \MongoId($category->getId());
        }

        $itProducts = $this->getMongoCollection()->find(array(
            'category' => array('$in' => $categoryIds)
        ));

        $products = array();
        foreach ($itProducts as $product) {
            $products[] = new Product(
                (string) $product['_id'], $product['isInStack'], $product['title'], $product['description'],
                $product['price'], $product['image'], $this->categoryManager->findOneById($product['category'])
            );
        }

        return $products;
    }

    public function addProduct(Product $product)
    {
        $productTransformer = new ProductTransformer($product);

        return $this->getMongoCollection()->insert($productTransformer->transformMongoJson());
    }

    protected function getMongoCollection()
    {
        return $this->mongoClient->selectCollection('products');
    }
}