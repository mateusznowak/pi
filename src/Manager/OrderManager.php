<?php

namespace Manager;

use DataTransformer\OrderTransformer;
use Model\Order;
use Model\User;

class OrderManager
{
    /** @var \MongoDB */
    protected $mongoClient;

    /** @var UserManager $userManager */
    protected $userManager;

    /** @var ProductManager $productManager */
    protected $productManager;

    public function __construct(\MongoDB $mongoClient, UserManager $userManager, ProductManager $productManager)
    {
        $this->mongoClient = $mongoClient;
        $this->userManager = $userManager;
        $this->productManager = $productManager;
    }

    public function order(Order $order)
    {
        $orderTransformer = new OrderTransformer($order);

        $this->getMongoCollection()->insert($orderTransformer->transformToMongoJson());
    }

    public function removeOrder($id)
    {
        return $this->getMongoCollection()->remove(array('_id' => new \MongoId($id)));
    }

    public function update(Order $order, array $updateFields = array())
    {
        return $this->getMongoCollection()->update(array('_id' => new \MongoId($order->getId())), array('$set' => $updateFields));
    }

    public function findOneById($id)
    {
        $order = $this->getMongoCollection()->findOne(array(
            '_id' => new \MongoId($id)
        ));

        $user = $this->userManager->findOneById($order['seller']);

        return new Order((string) $order['_id'], $order['name'], $order['email'], $order['phone'], $order['address'], $order['done'], $this->getProductsFromOrder($order['products']), $user);
    }

    public function findBySeller(User $user)
    {
        $itOrders = $this->getMongoCollection()->find(array(
            'seller' => new \MongoId($user->getId())
        ))->sort(array('done' => 1));

        $orderList = array();
        foreach($itOrders as $order)
        {
            $orderList[] = new Order((string) $order['_id'], $order['name'], $order['email'], $order['phone'], $order['address'], $order['done'], $this->getProductsFromOrder($order['products']), $user);
        }

        return $orderList;
    }

    protected function getProductsFromOrder(array $productOrder)
    {
        $productList = array();
        foreach ($productOrder as $productRef) {
            $productList[] = $this->productManager->findById((string) $productRef);
        }

        return $productList;
    }

    protected function getMongoCollection()
    {
        return $this->mongoClient->selectCollection('orders');
    }
}