<?php

namespace Manager;

use Model\User;

class UserManager
{
    /** @var \MongoDB */
    protected $mongoClient;

    public function __construct(\MongoDB $mongoClient)
    {
        $this->mongoClient = $mongoClient;
    }

    public function findOneById($id)
    {
        $u = $this->getMongoCollection()->findOne(array('_id' => $id));

        return new User((string) $u['_id'], $u['username'], $u['password'], $u['surname'], $u['lastname'], $u['pubToken']);
    }

    public function findOneByPubToken($pubToken)
    {
        $u = $this->getMongoCollection()->findOne(array('pubToken' => $pubToken));

        return new User((string) $u['_id'], $u['username'], $u['password'], $u['surname'], $u['lastname'], $u['pubToken']);
    }

    public function findAll()
    {

    }

    protected function getMongoCollection()
    {
        return $this->mongoClient->selectCollection('users');
    }
}