<?php

namespace Manager;

use Cocur\Slugify\Slugify;
use DataTransformer\CategoryTransformer;
use Model\Category;
use Model\User;
use Provider\MongoClientProvider;

class CategoryManager
{
    /** @var \MongoDB */
    protected $mongoClient;

    /** @var UserManager $userManager */
    protected $userManager;

    /** @var Slugify $slugify */
    protected $slugifier;

    public function __construct(\MongoDB $mongoClient, UserManager $userManager, Slugify $slugifier)
    {
        $this->mongoClient = $mongoClient;
        $this->userManager = $userManager;
        $this->slugifier = $slugifier;
    }

    public function findOneById($id)
    {
        $category = $this->getMongoCollection()->findOne(array(
            '_id' => new \MongoId($id)
        ));

        return new Category((string) $category['_id'], $category['title'], $category['slug'], $this->userManager->findOneById($category['userRef']));
    }

    public function update(Category $category, array $categoryUpdateFields = array())
    {
        return $this->getMongoCollection()->update(array('_id' => new \MongoId($category->getId())), array('$set' => $categoryUpdateFields));
    }

    public function findSellerCategoryByName(User $seller, $categoryName)
    {
        return $this->getMongoCollection()->find(array(
            'userRef' => new \MongoId($seller->getId()),
            'title' => $categoryName
        ));
    }

    public function findFeaturedBySeller(User $seller)
    {
        $categoryCollection = $this->getMongoCollection()->find(array(
            'userRef' => new \MongoId($seller->getId())
        ))->limit(10);
        $categoryDao = array();

        foreach ($categoryCollection as $category)
        {
            $categoryDao[] = new Category((string) $category['_id'], $category['title'], $category['slug'], $this->userManager->findOneById($category['userRef']));
        }

        return $categoryDao;
    }

    public function findBySeller(User $seller)
    {
        $categoryCollection = $this->getMongoCollection()->find(array(
            'userRef' => new \MongoId($seller->getId())
        ));
        $categoryDao = array();

        foreach ($categoryCollection as $category)
        {
            $categoryDao[] = new Category((string) $category['_id'], $category['title'], $category['slug'], $this->userManager->findOneById($category['userRef']));
        }

        return $categoryDao;
    }

    public function addCategory(User $seller, Category $category)
    {
        $category->setUser($seller);
        $category->setSlug($this->slugifier->slugify($category->getTitle()));

        $categoryTransformer = new CategoryTransformer($category);

        $this->getMongoCollection()->insert($categoryTransformer->transformMongoJson());

        return $categoryTransformer->transformMongoJson();
    }

    public function removeCategory($id)
    {
        return $this->getMongoCollection()->remove(array('_id' => new \MongoId($id)));
    }

    protected function getMongoCollection()
    {
        return $this->mongoClient->selectCollection('categories');
    }
}